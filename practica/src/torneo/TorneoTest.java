package torneo;

import junit.framework.TestCase;

public class TorneoTest extends TestCase {

	public void testTorneo (){
		
		Torneo torneo = new Torneo ();
		torneo.id = 1;
		
		Equipo equipo1 = new Equipo ();
		equipo1.nombre = "Valencia FC";
		Equipo equipo2 = new Equipo ();
		equipo2.nombre = "Atletico de Madrid";
		Equipo equipo3 = new Equipo ();
		equipo3.nombre = "FC Barcelona";
		Equipo equipo4 = new Equipo ();
		equipo4.nombre = "Real Madrid";
		
		torneo.equipo = new Equipo [4];
		torneo.equipo[0] = equipo1;
		torneo.equipo[1] = equipo2;
		torneo.equipo[2] = equipo3;
		torneo.equipo[3] = equipo4;
		
		assertTrue(torneo.equipo [0] == equipo1);
		assertTrue(torneo.equipo [1] == equipo2);
		assertTrue(torneo.equipo [2] == equipo3);
		assertTrue(torneo.equipo [3] == equipo4);
	}


}
